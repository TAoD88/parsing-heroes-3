![iconH](from readme/heroes-of-might-and-magic-iii-hd-edition-ico.png "HEROES3")

___
# Parsing Heroes 3

***Heroes of Might and Magic III***

[Самая популярная игра в своем жанре!!!](https://en.wikipedia.org/wiki/Heroes_of_Might_and_Magic_III) 


___
## Описание проекта
___
Этот проект задумался для сбора данных по известной игре. 

Нужно было получить данные по существам, артефактам, заклинаниям и героям из ***'HEROES 3'***

Задача состояла в том чтоб на выходе получить данные в формате *.JSON для дальнейшего использования их в другом проекте.

Для парсинга существует множество сообществ в открытом доступе ('Если у Вас попросят за это деньги, 
то вы имеете дело с ***мошенниками!***'')

После написания разных парсеров и попытке собрать все в одном месте я нашел интересный сайт https://hommbase.ru/ 
(который к сожалению сейчас не доступен, но парсить страницы можно и по ранее спарсеному коду)

Тут оставлю ссылочку работающий ресурс который доступен: [Horn of the Abyss](https://heroes.thelazy.net/index.php/List_of_creatures_(HotA))
___
## Используемый ресурс
___
Потребовалось всего 4 ссылки по которым я получил всю интересующую меня "Информацию":

1) https://hommbase.ru/mobs
2) https://hommbase.ru/artifacts
3) https://hommbase.ru/heroes
4) https://hommbase.ru/spells

Библиотеки:
  * BeautifulSoup
  * Lxml
  * Json
___
## Немного кода
___
- [X] Основной блок который запускает все остальные

```python
def main():
    ### Создаем список ссылок по которым будим соберать данные
    list_urls = ['https://hommbase.ru/mobs', 'https://hommbase.ru/artifacts', 'https://hommbase.ru/heroes',
                 'https://hommbase.ru/spells']
    ### Создаем словарь для удобной работы
    dict_names = {name.split('/')[-1]: name for name in list_urls}
    ### Используем модул save_html, который создает папки и сохраняем туда HTML
    for url in dict_names.keys():
        print(make_dir(url))
        # print(save_html(url)) # Отключил так как сайт перестал работать((
    ### Пробегаемся по 4 модулям, парсим данные из HTML и сохраняем в JSON
    for name, url in dict_names.items():
        globals()[name](name, url)
```

- [X] Пример части блока MOBS

```python
# Проверка на последний добавленный файл
def check_html(name) -> str:
    path = f"data/{name}"
    # Создаем список названий файлов в указанной нами папки который содержит все файлы формата "html"
    file_list = [i for i in os.listdir(path) if i.endswith("html")]
    for file in file_list:
        if name in file:
            file_pars = file  # Находим последний добавленный файл пробегаясь циклом по списку
            break
    # возращаем название файла
    return file_pars

# Парсим данные и сохраняем их в JSON
def spells(name,domen):
    # Создаем необходимые данные для красивого вывода
    start_time = time.time()
    cur_time = datetime.now().strftime("%d_%m_%Y")
    name_file = check_html(name)
    print(f'Для обработки был взят {name_file}')
    # Открываем нужный файл для сбора данных
    with open(f"data/{name}/{name_file}", encoding="utf-8") as file:
        src = file.read()

    soup = BS(src, "lxml")
    body = soup.find_all("div", class_="spell object-card")
    count = 0  # Счетчик
    list_spells = []  # Список куда будут сохранены все нужные данные
    # Пробегаемся циклом по всем элементам объекта
    for each in body:
        count += 1  # прибавляем с каждой итерацией для подсчета количества
        # Parsing information about a Spell
        engname = each.get('data-engname', "None").capitalize()
        rusname = each.get('data-rusname', "None").capitalize()
        basic = each.get('data-basic', "None").replace('<b>', '').replace('</b>', '').split("<a")
        advance = each.get('data-advance', "None").replace('<b>', '').replace('</b>', '')
        expert = each.get('data-expert', "None").replace('<b>', '').replace('</b>', '')
        class_spell = each.get('data-class', "None")
        element = each.get('data-element', "None")
        level = each.get('data-level', "None")
        value = each.get('data-value', "None").split('/')
        img = domen[:-(len(name)+1)] + each.find('img').get("data-src", "None")
        list_value = []
        if value:
            list_value = ['Basic: ' + value[0], 'Advance: ' + value[1], 'Expert: ' + value[2]]
        list_value = ' / '.join(list_value)
        list_basic = []
        for i in basic:
            if "Подробнее" not in i:
                list_basic.append(i)
        list_basic = '.'.join(list_basic)

        # Collecting all information about the spell
        list_spells.append(
            {
                'engname': engname,
                'rusname': rusname,
                'basic': list_basic,
                'advance': advance,
                'expert': expert,
                'class_spell': class_spell,
                'element': element,
                'level': level,
                'value': list_value,
                'img': img
            }
        )
```



