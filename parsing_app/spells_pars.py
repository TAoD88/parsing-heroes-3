import os
import json
import time
from bs4 import BeautifulSoup as BS
from datetime import datetime


# Проверка на последний добавленный файл
def check_html(name) -> str:
    path = f"data/{name}"
    # Создаем список названий файлов в указанной нами папки который содержит все файлы формата "html"
    file_list = [i for i in os.listdir(path) if i.endswith("html")]
    for file in file_list:
        if name in file:
            file_pars = file  # Находим последний добавленный файл пробегаясь циклом по списку
            break
    # возращаем название файла
    return file_pars

# Парсим данные и сохраняем их в JSON
def spells(name,domen):
    # Создаем необходимые данные для красивого вывода
    start_time = time.time()
    cur_time = datetime.now().strftime("%d_%m_%Y")
    name_file = check_html(name)
    print(f'Для обработки был взят {name_file}')
    # Открываем нужный файл для сбора данных
    with open(f"data/{name}/{name_file}", encoding="utf-8") as file:
        src = file.read()

    soup = BS(src, "lxml")
    body = soup.find_all("div", class_="spell object-card")
    count = 0  # Счетчик
    list_spells = []  # Список куда будут сохранены все нужные данные
    # Пробегаемся циклом по всем элементам объекта
    for each in body:
        count += 1  # прибавляем с каждой итерацией для подсчета количества
        # Parsing information about a Spell
        engname = each.get('data-engname', "None").capitalize()
        rusname = each.get('data-rusname', "None").capitalize()
        basic = each.get('data-basic', "None").replace('<b>', '').replace('</b>', '').split("<a")
        advance = each.get('data-advance', "None").replace('<b>', '').replace('</b>', '')
        expert = each.get('data-expert', "None").replace('<b>', '').replace('</b>', '')
        class_spell = each.get('data-class', "None")
        element = each.get('data-element', "None")
        level = each.get('data-level', "None")
        value = each.get('data-value', "None").split('/')
        img = domen[:-(len(name)+1)] + each.find('img').get("data-src", "None")
        list_value = []
        if value:
            list_value = ['Basic: ' + value[0], 'Advance: ' + value[1], 'Expert: ' + value[2]]
        list_value = ' / '.join(list_value)
        list_basic = []
        for i in basic:
            if "Подробнее" not in i:
                list_basic.append(i)
        list_basic = '.'.join(list_basic)

        # Collecting all information about the spell
        list_spells.append(
            {
                'engname': engname,
                'rusname': rusname,
                'basic': list_basic,
                'advance': advance,
                'expert': expert,
                'class_spell': class_spell,
                'element': element,
                'level': level,
                'value': list_value,
                'img': img
            }
        )

        print(f"Обработано Заклинание: {engname} ...")
    # Открываем файл в формате *.json и сохраняем все данные
    with open(f"data/{name}/{name}_{cur_time}.json", "w", encoding="utf-8") as file:
        json.dump(list_spells, file, indent=4, ensure_ascii=False)
    print(f"Заклинаний из Heroes of Might and Magic III All = {count}. Данные сохранены в data/{name}/{name}_{cur_time}.json")
    full_time = time.time() - start_time
    print(f"На работу скрипта потрачено: {full_time} сек.")
    print("#" * 100)

