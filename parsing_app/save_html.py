import requests
from datetime import datetime
import os


### создаем папку для файлов
def make_dir(url):
    name_for_dir = 'data/' + url.split('/')[-1]
    # Создаем место для хранения файлов если его нет
    if not os.path.isdir(name_for_dir):
        os.makedirs(name_for_dir)
        return f"Папка {url.split('/')[-1]} в директории parsing_app/data Cоздана!"
    return f"Папка {url.split('/')[-1]} в директории parsing_app/data уже существует"


### Сохраняем исходный код страницы для парсинга
def save_html(url):
    name = url.split('/')[-1]  # берем имя для названия страницы
    cur_time = datetime.now().strftime("%d_%m_%Y")  # берем дату для сохранения
    headers = {
        "accept": "*/*",
        "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36"
    }
    req = requests.get(url, headers=headers)  # получаем код исходной страницы
    src = req.text  # перегоняем в читаемый формат
    # Сохраняем в формате *.html с датой для возможности выбрать нужную
    with open(f"data/{name}/{name}_{cur_time}.html", "w", encoding="utf-8") as file:
        file.write(src)
    return f'{name}_{cur_time}.html был записан в data/{name}/'
