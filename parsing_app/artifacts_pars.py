import os
import json
import time
from bs4 import BeautifulSoup as BS
from datetime import datetime


# Проверка на последний добавленный файл
def check_html(name) -> str:
    path = f"data/{name}"
    # Создаем список названий файлов в указанной нами папки который содержит все файлы формата "html"
    file_list = [i for i in os.listdir(path) if i.endswith("html")]
    for file in file_list:
        if name in file:
            file_pars = file  # Находим последний добавленный файл пробегаясь циклом по списку
            break
    # возращаем название файла
    return file_pars


# Парсим данные и сохраняем их в JSON
def artifacts(name, domen):
    # Создаем необходимые данные для красивого вывода
    start_time = time.time()
    cur_time = datetime.now().strftime("%d_%m_%Y")
    name_file = check_html(name)
    print(f'Для обработки был взят {name_file}')
    # Открываем нужный файл для сбора данных
    with open(f"data/{name}/{name_file}", encoding="utf-8") as file:
        src = file.read()

    soup = BS(src, "lxml")
    body = soup.find_all("div", class_="artifact object-card")
    count = 0  # Счетчик
    list_artifact = []  # Список куда будут сохранены все нужные данные
    # Пробегаемся циклом по всем элементам объекта
    for each in body:
        count += 1  # прибавляем с каждой итерацией для подсчета количества
        # Parsing information about a artifact
        engname = each.get('data-engname', "None").title()
        rusname = each.get('data-rusname', "None").title()
        category = each.get('data-category', "None")
        id_artifact = each.get('data-id', "None")
        cost = each.get('data-cost', "None").replace('_', ' ').strip()
        sale = each.get('data-sale', "None").replace('_', ' ').strip()
        picture = each.get('data-picture', "None")
        slot = each.get('data-slot', "None")
        value = each.get('data-value', "None")
        set = each.get('data-set').replace('<br>', '').replace('<mark class=one>', '').replace('<b>', '').replace(
            '</b>', '').replace('</mark>', '').replace(']', '').replace('[', 'Собрав полный комплект вы получаете: ')
        if len(set) < 1:
            set = "None"
        level = each.get('data-level', "None")
        img = domen[:-(len(name) + 1)] + each.find('img').get("data-src", "None")
        bonus = each.get('data-bonus', "None").split('.')
        list_bonus = []
        for i in bonus:
            if 'Подробнее' not in i:
                list_bonus.append(i)
        list_bonus = '.'.join(list_bonus)

        # Collecting all information about the artifact
        list_artifact.append(
            {
                'engname': engname,
                'rusname': rusname,
                'category': category,
                'id_artifact': id_artifact,
                'cost': cost,
                'sale': sale,
                'picture': picture,
                'slot': slot,
                'value': value,
                'artifact_set': set,
                'level': level,
                'img': img,
                'bonus': list_bonus,
            }
        )

        print(f"Обработан {engname} артефакт ...")
    # Открываем файл в формате *.json и сохраняем все данные
    with open(f"data/{name}/{name}_{cur_time}.json", "w", encoding="utf-8") as file:
        json.dump(list_artifact, file, indent=4, ensure_ascii=False)
    print(
        f"Артефактов из Heroes of Might and Magic III All = {count}. Данные сохранены в data/{name}/{name}_{cur_time}.json")
    full_time = time.time() - start_time
    print(f"На работу скрипта потрачено: {full_time} сек.")
    print("#" * 100)
