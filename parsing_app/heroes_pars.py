import os
import json
import time
from bs4 import BeautifulSoup as BS
from datetime import datetime


# Проверка на последний добавленный файл
def check_html(name) -> str:
    path = f"data/{name}"
    # Создаем список названий файлов в указанной нами папки который содержит все файлы формата "html"
    file_list = [i for i in os.listdir(path) if i.endswith("html")]
    for file in file_list:
        if name in file:
            file_pars = file  # Находим последний добавленный файл пробегаясь циклом по списку
            break
    # возращаем название файла
    return file_pars


# Парсим данные и сохраняем их в JSON
def heroes(name, domen):
    # Создаем необходимые данные для красивого вывода
    start_time = time.time()
    cur_time = datetime.now().strftime("%d_%m_%Y")
    name_file = check_html(name)
    print(f'Для обработки был взят {name_file}')
    # Открываем нужный файл для сбора данных
    with open(f"data/{name}/{name_file}", encoding="utf-8") as file:
        src = file.read()

    soup = BS(src, "lxml")
    body = soup.find_all("div", class_="hero object-card")
    count = 0  # Счетчик
    list_heroes = []  # Список куда будут сохранены все нужные данные
    # Пробегаемся циклом по всем элементам объекта
    for each in body:
        count += 1  # прибавляем с каждой итерацией для подсчета количества
        # Parsing information about a heroes
        engname = each.get('data-engname', "None").capitalize()
        rusname = each.get('data-rusname', "None").capitalize()
        bio = each.get('data-bio', "None")
        heroes_rank = each.get('data-rank', "None")
        castle = each.get('data-castle', "None").capitalize()
        primary = each.get('data-primary', "None").split('/')
        race = each.get('data-race', "None")
        secondary1 = each.get('data-secondary1', "None")
        secondary2 = each.get('data-secondary2', "None")
        sex = each.get('data-sex', "None")
        specials = each.get('data-spec', "None").replace('<mark class=one>', '').replace('</mark>', '').split(".")
        spell = each.get('data-spell', "None")
        img = domen[:-(len(name) + 1)] + each.find('img').get("data-src", "None")
        list_primaty = []
        if primary:
            list_primaty = ['Атака: ' + primary[0], 'Защита: ' + primary[1], 'Сила магии: ' + primary[2],
                            'Знания: ' + primary[3]]
        list_primaty = ' / '.join(list_primaty)
        list_specials = []
        for i in specials:
            if "Подробнее" not in i:
                list_specials.append(i)
        list_specials = '.'.join(list_specials)
        list_secondary = []
        if len(secondary2) < 1:
            list_secondary = secondary1.capitalize()
        else:
            list_secondary = secondary1.capitalize() + ' / ' + secondary2.capitalize()

        # Collecting all information about the heroes
        list_heroes.append(
            {
                'engname': engname,
                'rusname': rusname,
                'castle': castle,
                'sex': sex,
                'heroes_rank': heroes_rank,
                'race': race,
                'bio': bio,
                'primary_heroes': list_primaty,
                'specials': list_specials,
                'spell': spell,
                'img': img,
                'list_secondary': list_secondary,
            }
        )

        print(f"Обработан Герой {engname} ...")
    # Открываем файл в формате *.json и сохраняем все данные
    with open(f"data/{name}/{name}_{cur_time}.json", "w", encoding="utf-8") as file:
        json.dump(list_heroes, file, indent=4, ensure_ascii=False)
    print(
        f"Героев из Heroes of Might and Magic III All = {count}. Данные сохранены в data/{name}/{name}_{cur_time}.json")

    full_time = time.time() - start_time
    print(f"На работу скрипта потрачено: {full_time} сек.")
    print("#" * 100)
