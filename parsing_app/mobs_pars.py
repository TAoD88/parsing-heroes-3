import os
import json
import time
from bs4 import BeautifulSoup as BS
from datetime import datetime


# Проверка на последний добавленный файл
def check_html(name) -> str:
    path = f"data/{name}"
    # Создаем список названий файлов в указанной нами папки который содержит все файлы формата "html"
    file_list = [i for i in os.listdir(path) if i.endswith("html")]
    for file in file_list:
        if name in file:
            file_pars = file  # Находим последний добавленный файл пробегаясь циклом по списку
            break
    # возращаем название файла
    return file_pars


# Парсим данные и сохраняем их в JSON
def mobs(name, domen):
    # Создаем необходимые данные для красивого вывода
    start_time = time.time()
    cur_time = datetime.now().strftime("%d_%m_%Y")
    name_file = check_html(name)
    print(f'Для обработки был взят {name_file}')
    # Открываем нужный файл для сбора данных
    with open(f"data/{name}/{name_file}", encoding="utf-8") as file:
        src = file.read()

    soup = BS(src, "lxml")
    body = soup.find_all("div", class_="mob object-card")
    count = 0  # Счетчик
    list_unit = []  # Список куда будут сохранены все нужные данные
    # Пробегаемся циклом по всем элементам объекта
    for each in body:
        count += 1  # прибавляем с каждой итерацией для подсчета количества
        # Parsing information about a unit
        engname = each.get('data-engname', "None").title()
        rusname = each.get('data-rusname', "None").title()
        attack = each.get('data-attack', "None")
        averagedamage = each.get('data-averagedamage', "None")
        castle = each.get('data-castle', "None").capitalize()
        cost = each.get('data-cost', "None").replace('_', ' ')
        damage = each.get('data-damage', "None")
        defense = each.get('data-defense', "None")
        growth = each.get('data-growth', "None")
        health = each.get('data-growth', "None")
        land = each.get('data-land', "None")
        level = each.get('data-level', "None")
        move = each.get('data-move', "None")
        number = each.get('data-number', "None")
        shoots = each.get('data-shoots', "None")
        speed = each.get('data-speed', "None")
        stat = each.get('data-stat', "None")
        up = each.get('data-up', "None")
        AI_value = each.get('data-value', "None")
        img = domen[:-(len(name) + 1)] + each.find('img').get("data-src", "None")
        ability = each.get('data-ability', "None").split('.')
        list_ability = []
        for i in ability:
            if '<a' not in i and 'Подробнее' not in i:
                list_ability.append(i)
        list_ability = '.'.join(list_ability)

        # Collecting all information about the unit
        list_unit.append(
            {
                'engname': engname,
                'rusname': rusname,
                'attack': attack,
                'averagedamage': averagedamage,
                'castle': castle,
                'cost': cost,
                'damage': damage,
                'defense': defense,
                'growth': growth,
                'health': health,
                'land': land,
                'level': level,
                'move': move,
                'number': number,
                'shoots': shoots,
                'speed': speed,
                'stat': stat,
                'up': up,
                'AI_value': AI_value,
                'img': img,
                'list_ability': list_ability

            }
        )
        print(f"Обработан {engname} юнит ...")
    # Открываем файл в формате *.json и сохраняем все данные
    with open(f"data/{name}/{name}_{cur_time}.json", "w", encoding="utf-8") as file:
        json.dump(list_unit, file, indent=4, ensure_ascii=False)
    print(
        f"Персонажей из Heroes of Might and Magic III All = {count}. Данные сохранены в data/{name}/{name}_{cur_time}.json")
    full_time = time.time() - start_time
    print(f"На работу скрипта потрачено: {full_time} сек.")
    print("#" * 100)
