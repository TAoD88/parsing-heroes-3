from save_html import save_html, make_dir
###  импортируем функции для globals()
from mobs_pars import mobs
from heroes_pars import heroes
from artifacts_pars import artifacts
from spells_pars import spells
###
import time

def main():
    start_time = time.time()
    ### Создаем список ссылок по которым будим соберать данные
    list_urls = ['https://hommbase.ru/mobs', 'https://hommbase.ru/artifacts', 'https://hommbase.ru/heroes',
                 'https://hommbase.ru/spells']
    ### Создаем словарь для удобной работы
    dict_names = {name.split('/')[-1]: name for name in list_urls}
    ### Используем модул save_html, который создает папки и сохраняем туда HTML
    for url in dict_names.keys():
        print(make_dir(url))
        # print(save_html(url)) # Отключил так как сайт перестал работать((
    ### Пробегаемся по 4 модулям, парсим данные из HTML и сохраняем в JSON
    for name, url in dict_names.items():
        globals()[name](name, url)

    print('Папки созданы и файлы сохранены ...')
    full_time = time.time() - start_time
    print(f'На работу скрипта потрачено: {full_time} сек.')



if __name__ == "__main__":
    main()
